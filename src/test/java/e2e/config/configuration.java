package e2e.config;

public final class configuration {

    public static final String URL = "http://localhost:3003/#/admin";
    public static final String ADMIN_USERNAME = "admin";
    public static final String ADMIN_PASSWORD = "password";

    public static final int SELENIDE_TIMEOUT = 20000;

}