package e2e.tests.automationintesting;

import automationintesting.LoginPage;
import e2e.config.configuration;
import org.junit.jupiter.api.Test;

public class LoginTest {

    @Test
    void login() {
        LoginPage.login(configuration.ADMIN_USERNAME, configuration.ADMIN_PASSWORD, configuration.URL);
    }
}
