package automationintesting;

import com.codeborne.selenide.Configuration;

public final class SelenideConfiguration {

    public static void configure() {
        Configuration.browser = System.getProperty("SELENIDE_BROWSER", "chrome");
        Configuration.browserSize = "1920x1080";
        Configuration.startMaximized = true;
        Configuration.holdBrowserOpen = true;
    }
}
