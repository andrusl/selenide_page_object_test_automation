package automationintesting.page;

import automationintesting.report.page.ReportPage;
import automationintesting.rooms.page.RoomsPage;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

public class NavbarPage {

    private static String _roomsTab = "//a[contains(text(),'Rooms')]";
    private static String _reportTab = "//a[@id='reportLink']";

    //TODO: log out

    public RoomsPage navRooms() {
        $(By.xpath(_roomsTab)).click();

        return page(RoomsPage.class);
    }

    public ReportPage navReport() {
        $(By.xpath(_reportTab)).click();

        return page(ReportPage.class);
    }

}
