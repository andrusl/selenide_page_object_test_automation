package automationintesting;

import automationintesting.rooms.page.RoomsPage;
import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;

public class LoginPage {

    private static final String _usernameField = "username";
    private static final String _passwordField = "password";
    private static final String _loginButton = "doLogin";
    private static final String _nextButton = "//*[@id=\"next\"]";
    private static final String _closeButton = "//*[@id=\"closeModal\"]";

    public static RoomsPage login(String userName, String password, String URL) {
        SelenideConfiguration.configure();
        open(URL);
        skipIntro();


        $(By.id(_usernameField)).setValue(userName);
        $(By.id(_passwordField)).setValue(password);
        $(By.id(_loginButton)).click();

        return page(RoomsPage.class);
    }

    private static void skipIntro() {
        while($(By.xpath(_nextButton)).is(Condition.exist)){
            $(By.xpath(_nextButton)).click();
        }

        if($(By.xpath(_closeButton)).is(Condition.exist)) {
            $(By.xpath(_closeButton)).click();
        }
    }
}
