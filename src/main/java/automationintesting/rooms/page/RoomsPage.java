package automationintesting.rooms.page;

import automationintesting.page.NavbarPage;
import automationintesting.rooms.model.RoomModel;
import automationintesting.rooms.room.page.RoomDetailsPage;
import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;


public class RoomsPage extends NavbarPage {

    private static final String _roomNumberField = "roomNumber";
    private static final String _typeField = "type";
    private static final String _accessibleField = "accessible";
    private static final String _wifiCheckbox = "wifiCheckbox";
    private static final String _tvCheckbox = "tvCheckbox";
    private static final String _viewsCheckbox = "viewsCheckbox";
    private static final String _createButton = "createRoom";
    private static final String _deleteRoomButton = "//*[@class=\"fa fa-remove roomDelete\"]";

    public RoomsPage createNewRoomByModel(RoomModel roomModel) {
        $(By.id(_roomNumberField)).setValue(String.valueOf(RoomModel._roomNumber));
        $(By.id(_typeField)).selectOption(roomModel._roomType);
        $(By.id(_accessibleField)).selectOption(roomModel._accessible);
        $(By.id(_createButton)).click();
        $(By.id("roomNumber" + Integer.toString(RoomModel._roomNumber))).shouldBe(exist);
        return page(RoomsPage.class);
    }

    //editRoom uses last roomNr from the model
    public RoomDetailsPage editRoom() {
        $(By.id(_roomNumberField + RoomModel._roomNumber)).click();
        return page(RoomDetailsPage.class);
    }


    public RoomDetailsPage editRoomByNumber(int roomNumber) {
        $(By.id(_roomNumberField + roomNumber)).click();
        return page(RoomDetailsPage.class);
    }

    public RoomsPage deleteAllRooms() {
        //Got to check for other element's existence first, otherwise it will continue too soon
        $(By.id(_createButton)).shouldBe(Condition.exist);

        //Try-catch, otherwise Exception when no element exists
        try {
            while ($(By.xpath(_deleteRoomButton)).is(Condition.exist)) {
                $(By.xpath(_deleteRoomButton)).click();
                $(By.id(_createButton)).shouldBe(Condition.exist);
            }
        } catch (NoSuchElementException e) {
            //No more elements
        } finally {
            return page(RoomsPage.class);
        }
    }
}
