package automationintesting.rooms.model;

import automationintesting.model.RoomType;
import automationintesting.model.TrueOrFalse;

public class RoomModel {
    public static int _roomNumber = 105;
    public static String _roomType = RoomType._suite;
    public String _accessible = TrueOrFalse._true;

    public static RoomModel familyRoom() {
        _roomType = RoomType._family;
        _roomNumber++;
        return new RoomModel();
    }

    public static RoomModel suiteRoom() {
        _roomType = RoomType._suite;
        _roomNumber++;
        return new RoomModel();
    }

    public static RoomModel singleRoom() {
        _roomType = RoomType._single;
        _roomNumber++;
        return new RoomModel();
    }

    public static RoomModel twinRoom() {
        _roomType = RoomType._twin;
        _roomNumber++;
        return new RoomModel();
    }

    public RoomModel setRoomNumber (int roomNumber){
        _roomNumber = roomNumber;
        return this;
    }

    //Returns last room number
    public static int getRoomNumber(){
        return _roomNumber;
    };

    //TODO
    //_roomNumber, RoomType._family, _beds, TrueOrFalse._true, "test"
}
