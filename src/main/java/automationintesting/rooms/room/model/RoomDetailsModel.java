package automationintesting.rooms.room.model;

import automationintesting.model.TrueOrFalse;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class RoomDetailsModel {
    public static String _firstName;
    public static String _lastName;
    public static int _price;
    public static String _depositPaid;
    public static String _checkInDate;
    public static String _checkOutDate;
    public static String _checkInDay;
    public static String _checkOutDay;
    public static Calendar _today;
    public static SimpleDateFormat _format;

    public static RoomDetailsModel defaultBooking () {
        if (_today == null){
            _today = Calendar.getInstance();
            _today.setTime(new Date());
        } else {
            Calendar beginningOfMonth = Calendar.getInstance();
            beginningOfMonth.setTime(_today.getTime());
            beginningOfMonth.roll(Calendar.DATE, 2);
            if (_today.after(beginningOfMonth)) {
                _today.roll(Calendar.DATE, 2);
            } else {
                _today.roll(Calendar.DATE, 1);
            }
        }

        _firstName = "Nacho";
        _lastName = "Business";
        _price = 101;
        _depositPaid = new TrueOrFalse()._true;
        _format = new SimpleDateFormat("yyyy-MM-dd");

        _checkInDate = _format.format(_today.getTime());
        _checkInDay = new SimpleDateFormat("dd").format(_today.getTime());
        _today.roll(Calendar.DATE, 1);
        _checkOutDate = _format.format(_today.getTime());
        _checkOutDay = new SimpleDateFormat("dd").format(_today.getTime());

        return new RoomDetailsModel();
    }

}
