package automationintesting.rooms.room.page;

import automationintesting.page.NavbarPage;
import automationintesting.rooms.room.model.RoomDetailsModel;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;
@SuppressWarnings("AccessStaticViaInstance")
public class RoomDetailsPage extends NavbarPage {
    private static final String _firstNameField = "firstname";
    private static final String _lastNameField = "lastname";
    private static final String _priceField = "totalprice";
    private static final String _depositPaidField = "depositpaid";
    private static final String _depositPaidOption = "//option[@value='_depositPaid']";
    private static final String _checkInContainer = "//*[contains(@class, 'checkin')]//*[@class='react-datepicker__input-container']/descendant::node()";
    private static final String _checkInOption = "//*[@class=\"react-datepicker-popper\"]/descendant::node()/*[not(contains(@class,'outside-month'))][contains(@class, 'Day')]";
    private static final String _checkOutContainer = "//*[contains(@class, 'checkout')]//*[@class='react-datepicker__input-container']/descendant::node()";
    private static final String _checkOutOption = "//*//*[@class=\"react-datepicker-popper\"]/descendant::node()/*[not(contains(@class,'outside-month'))][contains(@class, 'Day')]";
    private static final String _checkOutColumn = "//*[@class=\"col-sm-2\"]/*[contains(text(), \"date\")]";
    private static final String _createButton = "createBooking";

    /* TODO: Täiendada
    public RoomDetailsPage createNewBooking(String firstName,
                         String lastName,
                         Integer price,
                         TrueOrFalse depositPaid,
                         String checkInDate,
                         String checkOutDate) {
        $(By.id(_firstNameField)).setValue(firstName);
        $(By.id(_lastNameField)).setValue(lastName);
        $(By.id(_priceField)).setValue(String.valueOf(price));
        $(By.id(_depositPaidField)).setValue(String.valueOf(depositPaid));
        $(By.xpath(_checkInOption)).setValue(checkInDate);
        $(By.xpath(_checkOutOption)).setValue(checkOutDate);
        $(By.id(_createButton)).click();

        return page(RoomDetailsPage.class);
    }*/

    // TODO: Lisada meetod, mis vastavalt sisendiks antud kuupäevale suudab ka kalendrit kerida, et vastav kuupäev valida.
    public RoomDetailsPage addBookingByModel(RoomDetailsModel roomDetailsModel) {
        $(By.id(_firstNameField)).setValue(roomDetailsModel._firstName);
        $(By.id(_lastNameField)).setValue(roomDetailsModel._lastName);
        $(By.id(_priceField)).setValue(String.valueOf(roomDetailsModel._price));
        $(By.id(_depositPaidField)).click();
        $(By.xpath(_depositPaidOption.replace("_depositPaid", roomDetailsModel._depositPaid))).click();
        $(By.xpath(_checkInContainer)).click();
        $(By.xpath(_checkInOption.replace("Day","0" + RoomDetailsModel._checkInDay))).click();
        $(By.xpath(_checkOutContainer)).click();
        $(By.xpath(_checkOutOption.replace("Day","0" + RoomDetailsModel._checkOutDay))).click();
        $(By.id(_createButton)).click();
        $(By.xpath(_checkOutColumn.replace("date", roomDetailsModel._checkOutDate))).shouldBe(exist);
        return page(RoomDetailsPage.class);
    }

    public RoomDetailsPage addBookingByModelAndAmount(RoomDetailsModel roomDetailsModel, int amount) {
        int i = 0;

        if (i < amount) {
            addBookingByModel(roomDetailsModel);
            addBookingByModelAndAmount(roomDetailsModel.defaultBooking(), amount - 1);
        }

        return page(RoomDetailsPage.class);
    }
}
