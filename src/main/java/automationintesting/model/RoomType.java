package automationintesting.model;

public class RoomType {
    public static final String _single = "Single";
    public static final String _twin = "Twin";
    public static final String _family = "Family";
    public static final String _suite = "Suite";

}
